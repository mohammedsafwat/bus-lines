//
//  TestConfiguration.swift
//  BusLinesTests
//
//  Created by Mohammed Safwat on 6/11/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

struct TestConfiguration {
    struct Stubs {
        static let ValidStationData = "StubValidStationData"
        static let InvalidStationData = "StubInvalidStationData"
    }
    
    struct CoreData {
        struct DataModel {
            static let Name = "BusLines"
        }
    }
}
