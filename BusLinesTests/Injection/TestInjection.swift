//
//  TestInjection.swift
//  BusLinesTests
//
//  Created by Mohammed Safwat on 6/11/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
@testable import BusLines

class TestInjection {
    class func provideMockStationTimetableData() -> Timetable {
        let arrivalRouteItem = RouteItem(id: 9437, name: "Warszawa Młociny", address: "ul. Kasprowicza 145", fullAddress: "ul. Kasprowicza 145, 01-949 Warszawa, Poland", coordinates: Coordinates(latitude: 52.290770981763, longitude: 20.927733164349))
        let arrivalItem = TimetableItem(briefRoute: "Warszawa Młociny → Lodz (Łódź) Fabryczn", dateTime: DateTime(timestamp: 1528686000, timezone: "GMT+02:00"), direction: "Berlin TXL, E", lineCode: "N60", routes: [arrivalRouteItem, arrivalRouteItem])
        let arrivals = [arrivalItem]
        
        
        let departureRouteItem = RouteItem(id: 1, name: "Berlin central bus station", address: "Masurenallee 4-6", fullAddress: "Masurenallee 4-6, 14057 Berlin, Germany", coordinates: Coordinates(latitude: 52.507171, longitude: 13.279399))
        let departureItem = TimetableItem(briefRoute: "Berlin central bus station → Berlin TXL", dateTime: DateTime(timestamp: 1528686000, timezone: "GMT+02:00"), direction: "Kiel", lineCode: "N50", routes: [departureRouteItem, departureRouteItem])
        let departures = [departureItem]
        
        let timetable = Timetable(stationId: "2", arrivals: arrivals, departures: departures)
        return timetable
    }
}
