//
//  StationTimetableViewModelSpec.swift
//  BusLinesTests
//
//  Created by Mohammed Safwat on 6/11/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Quick
import Nimble
@testable import BusLines

class StationTimetableViewModelSpec: QuickSpec {
    
    override func spec() {
        var viewModel: StationTimetableViewModel?
        var stationTimetableRepository: StationTimetableRepository?
        
        let stationId = "1"

        let remoteDataSource = StationTimetableFakeRemoteDataSource(apiClient: RESTApiClient(headers: [:]), requestUrl: "")
        let localDataSource = StationTimetableFakeLocalDataSource(coreDataManager: FakeCoreDataManager(modelName: TestConfiguration.CoreData.DataModel.Name))
        
        describe("station time table view model") {
            beforeEach {
                remoteDataSource.fetchStationTimetableIsCalled = false
                localDataSource.fetchStationTimetableIsCalled = false
                stationTimetableRepository = StationTimetableRepository(remoteDataSource: remoteDataSource, localDataSource: localDataSource)
                viewModel = StationTimetableViewModel(stationTimetableDataSource: stationTimetableRepository)
                viewModel?.timetable = TestInjection.provideMockStationTimetableData()
            }
            
            context("when `fetchStationTimetable` is called and `foreUpdate` is true") {
                beforeEach {
                    viewModel?.fetchStationTimetable(stationId: stationId, forceUpdate: true, showLoadingUI: true)
                }
                
                it("should call `fetchStationTimetable` on remote data source only", closure: {
                    expect(remoteDataSource.fetchStationTimetableIsCalled).to(beTrue())
                    expect(localDataSource.fetchStationTimetableIsCalled).to(beFalse())
                })
            }
            
            context("when `fetchStationTimetable` is called and `foreUpdate` is false") {
                beforeEach {
                    viewModel?.fetchStationTimetable(stationId: stationId, forceUpdate: false, showLoadingUI: true)
                }
                
                it("should call `fetchStationTimetable` on local data source and then remote data source", closure: {
                    expect(localDataSource.fetchStationTimetableIsCalled).to(beTrue())
                    expect(remoteDataSource.fetchStationTimetableIsCalled).to(beTrue())
                })
            }
            
            context("fetch numberOfRows for departures segment") {
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.departures
                }
                
                it("should return the same number of departure Timetable Items") {
                    expect(viewModel?.numberOfRows).to(equal(viewModel?.timetable?.departures?.count))
                }
            }
            
            context("fetch numberOfRows for arrivals segment") {
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.arrivals
                }
                
                it("should return the same number of arrivals Timetable Items") {
                    expect(viewModel?.numberOfRows).to(equal(viewModel?.timetable?.arrivals?.count))
                }
            }
            
            context("fetch numberOfRows when departures array is empty") {
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.departures
                    viewModel?.timetable?.departures = []
                }
                
                it("should return zero numberOfRows") {
                    expect(viewModel?.numberOfRows).to(equal(0))
                }
            }
            
            context("fetch numberOfRows when arrivals array is empty") {
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.arrivals
                    viewModel?.timetable?.arrivals = []
                }
                
                it("should return zero numberOfRows") {
                    expect(viewModel?.numberOfRows).to(equal(0))
                }
            }
            
            context("fetch numberOfRows when departures array is nil") {
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.departures
                    viewModel?.timetable?.departures = nil
                }
                
                it("should return zero numberOfRows") {
                    expect(viewModel?.numberOfRows).to(equal(0))
                }
            }
            
            context("fetch numberOfRows when arrivals array is nil") {
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.arrivals
                    viewModel?.timetable?.arrivals = nil
                }
                
                it("should return zero numberOfRows") {
                    expect(viewModel?.numberOfRows).to(equal(0))
                }
            }
            
            context("fetch `stationTimetableItemViewModel` for a departure") {
                let departureItemIndex = 0
                var departureItemViewModel: StationTimetableItemViewModel?
                
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.departures
                }
                
                it("should return a view model for the departure item") {
                    departureItemViewModel = viewModel?.stationTimetableItemViewModel(for: departureItemIndex)
                    expect(departureItemViewModel).toNot(beNil())
                }
            }
            
            context("fetch `stationTimetableItemViewModel` for an arrival") {
                let arrivalItemIndex = 0
                var arrivalItemViewModel: StationTimetableItemViewModel?
                
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.arrivals
                }
                
                it("should return a view model for the arrival item") {
                    arrivalItemViewModel = viewModel?.stationTimetableItemViewModel(for: arrivalItemIndex)
                    expect(arrivalItemViewModel).toNot(beNil())
                }
            }
            
            context("fetch `stationTimetableItemViewModel` for a departure when the departures array is nil") {
                let departureItemIndex = 0
                var departureItemViewModel: StationTimetableItemViewModel?
                
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.departures
                    viewModel?.timetable?.departures = nil
                }
                
                it("should return a nil view model for the departure item") {
                    departureItemViewModel = viewModel?.stationTimetableItemViewModel(for: departureItemIndex)
                    expect(departureItemViewModel).to(beNil())
                }
            }
            
            context("fetch `stationTimetableItemViewModel` for an arrival when the arrivals array is nil") {
                let arrivalItemIndex = 0
                var arrivalItemViewModel: StationTimetableItemViewModel?
                
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = StationTimetableSegment.arrivals
                    viewModel?.timetable?.arrivals = nil
                }
                
                it("should return a nil view model for the arrival item") {
                    arrivalItemViewModel = viewModel?.stationTimetableItemViewModel(for: arrivalItemIndex)
                    expect(arrivalItemViewModel).to(beNil())
                }
            }
            
            context("fetch `stationTimetableItemViewModel` when `selectedStationTimetableSegment` is nil") {
                beforeEach {
                    viewModel?.selectedStationTimetableSegment = nil
                }
                
                it("should return nil stationTimetableItemViewModel") {
                    expect(viewModel?.stationTimetableItemViewModel(for: 0)).to(beNil())
                }
            }
        }
    }
}
