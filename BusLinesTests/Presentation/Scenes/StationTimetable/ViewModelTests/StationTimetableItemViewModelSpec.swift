//
//  StationTimetableItemViewModelSpec.swift
//  BusLinesTests
//
//  Created by Mohammed Safwat on 6/11/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Quick
import Nimble
@testable import BusLines

class StationTimetableItemViewModelSpec: QuickSpec {
    
    override func spec() {
        var viewModel: StationTimetableItemViewModel?
        var mockDepartureTimetableItem: TimetableItem?
        var mockArrivalTimetableItem: TimetableItem?
        
        describe("station time table item view model") {
            context("fetch properties from view model") {
                beforeEach {
                    mockDepartureTimetableItem = TestInjection.provideMockStationTimetableData().departures?.first
                    mockArrivalTimetableItem = TestInjection.provideMockStationTimetableData().arrivals?.first
                    
                    viewModel = StationTimetableItemViewModel(stationTimetableItem: mockDepartureTimetableItem!, stationTimetableSegment: .departures)
                }
                
                it("should return direction title equals to 'To'") {
                    expect(viewModel?.directionTitle).to(equal("To"))
                }
                
                it("should return direction details string prefixed by 'Direction' word") {
                    expect(viewModel?.directionDetails).to(contain("Direction "))
                    expect(viewModel?.directionDetails).to(equal("Direction " + (mockDepartureTimetableItem?.direction!)!))
                }
                
                it("should return lineCode string prefixed by 'Line' word") {
                    expect(viewModel?.lineCode).to(equal("Line " + (mockDepartureTimetableItem?.lineCode!)!))
                }
                
                it("should return route string joined by '→' character") {
                    expect(viewModel?.route).to(contain("→"))
                }
                
                it("should return an empty route string if the routes array is nil") {
                    viewModel?.stationTimetableItem.routes = nil
                    expect(viewModel?.route).to(beEmpty())
                }
                
                it("should return an empty direction details string if direction property is nil") {
                    viewModel?.stationTimetableItem.direction = nil
                    expect(viewModel?.directionDetails).to(beEmpty())
                }
                
                it("should return an empty lineCode string if lineCode property is nil") {
                    viewModel?.stationTimetableItem.lineCode = nil
                    expect(viewModel?.lineCode).to(beEmpty())
                }
                
                it("should return departure time string formatted with the correct timezone") {
                    expect(viewModel?.time).to(equal("05:00"))
                }
                
                it("should return an empty departure time string if the timezone or the timestamp are nil") {
                    viewModel?.stationTimetableItem.dateTime?.timestamp = nil
                    expect(viewModel?.time).to(beEmpty())
                }
                
                it("should return departure date string formatted with the correct timezone") {
                    expect(viewModel?.date).to(equal("Mon, 11 Jun 2018"))
                }
                
                it("should return an adeparture date string if the timezone or the timestamp are nil") {
                    viewModel?.stationTimetableItem.dateTime?.timestamp = nil
                    expect(viewModel?.date).to(beEmpty())
                }
            }
            
            context("view model for an arrival item") {
                beforeEach {
                    viewModel = StationTimetableItemViewModel(stationTimetableItem: mockArrivalTimetableItem!, stationTimetableSegment: .arrivals)
                }
                
                it("should return direction title with a string equals to 'From'") {
                    expect(viewModel?.directionTitle).to(equal("From"))
                }
            }
        }
    }
}

