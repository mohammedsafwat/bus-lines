//
//  StationTimetableRepositorySpec.swift
//  BusLinesTests
//
//  Created by Mohammed Safwat on 6/11/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Quick
import Nimble
@testable import BusLines

class StationTimetableRepositorySpec: QuickSpec {
    
    override func spec() {
        super.spec()
        
        let stationId = "1"
        let mockStationTimetableData = TestInjection.provideMockStationTimetableData()
        
        var stationTimetableRepository: StationTimetableRepository?
        let remoteDataSource = StationTimetableFakeRemoteDataSource(apiClient: RESTApiClient(headers: [:]), requestUrl: "")
        let localDataSource = StationTimetableFakeLocalDataSource(coreDataManager: FakeCoreDataManager(modelName: TestConfiguration.CoreData.DataModel.Name))
        
        var fetchedStationTimetableData: Timetable?
        var fetchedError: DataError?
        
        describe("station time table repository") {
            beforeEach {
                fetchedStationTimetableData = nil
                fetchedError = nil
                remoteDataSource.fetchStationTimetableIsCalled = false
                localDataSource.fetchStationTimetableIsCalled = false
                stationTimetableRepository = StationTimetableRepository(remoteDataSource: remoteDataSource, localDataSource: localDataSource)
            }
            
            context("when `fetchStationTimetable` is called") {
                beforeEach {
                    remoteDataSource.dataIsAvailable = true
                    stationTimetableRepository?.fetchStationTimetable(stationId: stationId)
                }
                
                it("should call `fetchStationTimetable` on local data source") {
                    expect(localDataSource.fetchStationTimetableIsCalled).to(beTrue())
                }
                
                it("should call `fetchStationTimetable` on remote data source") {
                    expect(remoteDataSource.fetchStationTimetableIsCalled).to(beTrue())
                }
            }
            
            context("when `fetchStationTimetable` is called twice") {
                beforeEach {
                    remoteDataSource.dataIsAvailable = true
                    stationTimetableRepository?.fetchStationTimetable(stationId: stationId, onSuccess: { (timetable) in
                        fetchedStationTimetableData = timetable
                    })
                }
                
                it("should cache station time table after first call to API") {
                    expect(remoteDataSource.fetchStationTimetableIsCalled).to(beTrue())
                    expect(fetchedStationTimetableData?.arrivals?.first?.briefRoute).to(equal(mockStationTimetableData.arrivals?.first?.briefRoute))
                    
                    // Call fetchStationTimetable the second time. It should not call
                    // `fetchStationTimetable` on remoteDataSource and just return
                    // the data from the cache.
                    remoteDataSource.fetchStationTimetableIsCalled = false
                    fetchedStationTimetableData = nil
                    stationTimetableRepository?.fetchStationTimetable(stationId: stationId, onSuccess: { (timetable) in
                        fetchedStationTimetableData = timetable
                    })
                    expect(remoteDataSource.fetchStationTimetableIsCalled).to(beFalse())
                    expect(fetchedStationTimetableData?.arrivals?.first?.briefRoute).to(equal(mockStationTimetableData.arrivals?.first?.briefRoute))
                }
            }
            
            context("when fetch station time table is called and data is not available from remote data source", closure: {
                beforeEach {
                    remoteDataSource.dataIsAvailable = false
                    stationTimetableRepository?.fetchStationTimetable(stationId: stationId, onSuccess: { (timetable) in
                        fetchedStationTimetableData = timetable
                    }, onError: { (error) in
                        fetchedError = error
                    })
                }
                
                it("should call local data source", closure: {
                    expect(localDataSource.fetchStationTimetableIsCalled).to(beTrue())
                })
                
                it("should call remote data source", closure: {
                    expect(remoteDataSource.fetchStationTimetableIsCalled).to(beTrue())
                })
                
                it("should return an error", closure: {
                    expect(fetchedError).toNot(beNil())
                })
                
                it("should not set any value to the fetchedStationTimetableData object", closure: {
                    expect(fetchedStationTimetableData).to(beNil())
                })
            })
            
            context("when fetch station time table is called, cache is empty and local data source has data", closure: {
                beforeEach {
                    localDataSource.dataIsAvailable = true
                    stationTimetableRepository?.fetchStationTimetable(stationId: stationId, onSuccess: { (timetable) in
                        fetchedStationTimetableData = timetable
                    })
                }
                
                it("should fetch data from local data source", closure: {
                    expect(localDataSource.fetchStationTimetableIsCalled).to(beTrue())
                    expect(remoteDataSource.fetchStationTimetableIsCalled).to(beFalse())
                    expect(fetchedStationTimetableData?.arrivals?.first?.briefRoute).to(equal(mockStationTimetableData.arrivals?.first?.briefRoute))
                })
            })
            
            context("when `saveStationTimetable` is called", closure: {
                beforeEach {
                    stationTimetableRepository?.saveStationTimetable(stationId: stationId, stationTimetable: mockStationTimetableData)
                }
                
                it("should call `saveStationTimetable` on remote and local data sources", closure: {
                    expect(remoteDataSource.saveStationTimetableIsCalled).to(beTrue())
                    expect(localDataSource.saveStationTimetableIsCalled).to(beTrue())
                })
                
                it("should update the cache after calling `saveStationTimetable`", closure: {
                    expect(stationTimetableRepository?.cachedTimetableData.keys.count).to(equal(1))
                    expect(stationTimetableRepository?.cachedTimetableData.keys.first).to(equal(stationId))
                })
            })
            
            context("when `deleteStationTimetable` is called", closure: {
                beforeEach {
                    stationTimetableRepository?.deleteStationTimetable(stationId: stationId)
                }
                
                it("should call `deleteStationTimetable` on remote and local data sources", closure: {
                    expect(remoteDataSource.deleteStationTimetableIsCalled).to(beTrue())
                    expect(localDataSource.deleteStationTimetableIsCalled).to(beTrue())
                })
                
                it("should clear the cache after calling `deleteStationTimetable`", closure: {
                    expect(stationTimetableRepository?.cachedTimetableData.count).to(equal(0))
                })
            })
        }
    }
}
