//
//  StationTimetableLocalDataSourceSpec.swift
//  BusLinesTests
//
//  Created by Mohammed Safwat on 6/11/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Quick
import Nimble
@testable import BusLines

class StationTimetableLocalDataSourceSpec: QuickSpec {
    
    override func spec() {
        super.spec()
        
        var fetchedStationTimetableData: Timetable?
        var fetchedDataError: DataError?
        var stationTimetableLocalDataSource: StationTimetableLocalDataSource?
        
        let fakeCoreDataManager = FakeCoreDataManager(modelName: TestConfiguration.CoreData.DataModel.Name)
        let mockStationTimetableData = TestInjection.provideMockStationTimetableData()
        
        describe("station time table local data source") {
            beforeEach {
                fetchedStationTimetableData = nil
                fetchedDataError = nil
                
                stationTimetableLocalDataSource = StationTimetableLocalDataSource(coreDataManager: fakeCoreDataManager)
                
                stationTimetableLocalDataSource?.deleteAllStationTimetables()
            }
            
            context("fetch a station time table record that does not exist", closure: {
                beforeEach {
                    stationTimetableLocalDataSource?.fetchStationTimetable(stationId: "100", onError: { (dataError) in
                        fetchedDataError = dataError
                    })
                }
                
                it("should return an error", closure: {
                    expect(fetchedDataError).toEventuallyNot(beNil())
                })
            })
            
            context("fetch a station time table record aftet saving it", closure: {
                beforeEach {
                    stationTimetableLocalDataSource?.saveStationTimetable(stationId: mockStationTimetableData.stationId!, stationTimetable: mockStationTimetableData)
                    stationTimetableLocalDataSource?.fetchStationTimetable(stationId: mockStationTimetableData.stationId!, onSuccess: { (timetable) in
                        fetchedStationTimetableData = timetable
                    })
                }
                
                it("should not return an error if the managedObjectContext successfully saves the record to core data") {
                    expect(fetchedDataError).to(beNil())
                }
                
                it("should return the same record that was saved into core data") {
                    expect(fetchedStationTimetableData).toEventuallyNot(beNil(), timeout: 2)
                    expect(fetchedStationTimetableData?.arrivals?.first?.briefRoute).toEventually(equal(mockStationTimetableData.arrivals?.first?.briefRoute), timeout: 2)
                }
            })
            
            context("fetch a station time table record after deleting it", closure: {
                beforeEach {
                    stationTimetableLocalDataSource?.saveStationTimetable(stationId: mockStationTimetableData.stationId!, stationTimetable: mockStationTimetableData)
                    stationTimetableLocalDataSource?.deleteStationTimetable(stationId: mockStationTimetableData.stationId!)
                    stationTimetableLocalDataSource?.fetchStationTimetable(stationId: mockStationTimetableData.stationId!, onSuccess: { (timetable) in
                        fetchedStationTimetableData = timetable
                    })
                }
                
                it("should not return any time table record", closure: {
                    expect(fetchedStationTimetableData).toEventually(beNil(), timeout: 2)
                })
            })
        }
    }
}

