//
//  StationTimetableRemoteDataSourceSpec.swift
//  BusLinesTests
//
//  Created by Mohammed Safwat on 6/11/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Quick
import Nimble
@testable import BusLines

class StationTimetableRemoteDataSourceSpec: QuickSpec {
    
    override func spec() {
        super.spec()
        
        var fetchedStationTimetableData: Timetable?
        var fetchedDataError: DataError?
        var stationTimetableRemoteDataSource: StationTimetableRemoteDataSource?
        
        let apiClient = RESTApiClient(headers: [:])
        let requestUrl = "http://api.mobile.staging.mfb.io/mobile/v1/network/station/{station_id}/timetable"
        
        describe("station time table remote data source") {
            beforeEach {
                fetchedStationTimetableData = nil
                fetchedDataError = nil
                stationTimetableRemoteDataSource = StationTimetableRemoteDataSource(apiClient: apiClient, requestUrl: requestUrl)
            }
            
            context("when fetching station time table with an id that exists") {
                beforeEach {
                    self.stub(urlString: requestUrl, jsonFileName: TestConfiguration.Stubs.ValidStationData)
                    stationTimetableRemoteDataSource?.fetchStationTimetable(stationId: "1", onSuccess: { (timetable) in
                        fetchedStationTimetableData = timetable
                    })
                }
                
                it("correctly parses station time table data", closure: {
                    expect(fetchedStationTimetableData).toEventuallyNot(beNil(), timeout: 10)
                    expect(fetchedStationTimetableData?.arrivals?.first?.briefRoute).to(equal("Warszawa Młociny → Lodz (Łódź) Fabryczna → Poznań → Berlin SXF → Berlin central bus station"))
                    expect(fetchedStationTimetableData?.arrivals?.first?.direction).to(equal("Berlin central bus station"))
                    expect(fetchedStationTimetableData?.arrivals?.first?.dateTime?.timestamp).to(equal(1528686000))
                    expect(fetchedStationTimetableData?.arrivals?.first?.lineCode).to(equal("P3"))
                    expect(fetchedStationTimetableData?.arrivals?.first?.routes?.first?.name).to(equal("Warszawa Młociny"))
                    
                    expect(fetchedStationTimetableData?.departures?.first?.briefRoute).to(equal("Berlin central bus station → Berlin TXL, E → Hamburg → Kiel"))
                    expect(fetchedStationTimetableData?.departures?.first?.direction).to(equal("Kiel"))
                    expect(fetchedStationTimetableData?.departures?.first?.dateTime?.timestamp).to(equal(1528686000))
                    expect(fetchedStationTimetableData?.departures?.first?.lineCode).to(equal("N50"))
                    expect(fetchedStationTimetableData?.departures?.first?.routes?.first?.name).to(equal("Berlin central bus station"))
                })
            }
            
            context("when fetching station time table with an id that does not exist", closure: {
                beforeEach {
                    self.stub(urlString: requestUrl, jsonFileName: TestConfiguration.Stubs.InvalidStationData)
                    stationTimetableRemoteDataSource?.fetchStationTimetable(stationId: "100", onError: { (dataError) in
                        fetchedDataError = dataError
                    })
                }
                
                it("should return an error", closure: {
                    expect(fetchedDataError).toEventuallyNot(beNil(), timeout: 10)
                    expect(fetchedDataError?.type).toEventually(equal(DataErrorType.parse), timeout: 10)
                })
            })
            
            context("when fetching station time table is not successful", closure: {
                let error = NSError(domain: "com.safwat.BusLines", code: 401, userInfo: nil)
                
                beforeEach {
                    self.stub(urlString: requestUrl, error: error)
                    
                    stationTimetableRemoteDataSource?.fetchStationTimetable(stationId: "1", onError: { (dataError) in
                        fetchedDataError = dataError
                    })
                }
                
                it("should return an error", closure: {
                    expect(fetchedDataError).toEventuallyNot(beNil(), timeout: 10)
                    expect(fetchedDataError?.message).to(equal(error.localizedDescription))
                })
            })
        }
    }
}

