//
//  StationTimetableFakeRemoteDataSource.swift
//  BusLinesTests
//
//  Created by Mohammed Safwat on 6/11/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
@testable import BusLines

class StationTimetableFakeRemoteDataSource: StationTimetableRemoteDataSource {
    var dataIsAvailable: Bool = false
    var fetchStationTimetableIsCalled: Bool = false
    var saveStationTimetableIsCalled: Bool = false
    var deleteStationTimetableIsCalled: Bool = false
    var refreshStationTimetableIsCalled: Bool = false
    
    override func fetchStationTimetable(stationId: String, onSuccess: SuccessCallBack?, onError: ErrorCallBack?) {
        fetchStationTimetableIsCalled = true
        
        if dataIsAvailable {
            onSuccess?(TestInjection.provideMockStationTimetableData())
        } else {
            let dataError = DataError(type: .requestFailed)
            onError?(dataError)
        }
    }
    
    override func saveStationTimetable(stationId: String, stationTimetable: Timetable, onError: ErrorCallBack?) {
        saveStationTimetableIsCalled = true
    }
    
    override func deleteStationTimetable(stationId: String, onError: ErrorCallBack?) {
        deleteStationTimetableIsCalled = true
    }
    
    override func refreshStationTimetable(forceUpdate: Bool) {
        refreshStationTimetableIsCalled = true
    }
}
