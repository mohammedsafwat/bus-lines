//
//  Configuration.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

struct Configuration {
    struct API {
        static let Headers = ["X-Api-Authentication":"intervIEW_TOK3n"]
        
        struct Endpoints {
            static let BaseURL = "http://api.mobile.staging.mfb.io"
            static let Path = "/mobile/v1/network/station/{station_id}/timetable"
        }
        
        struct Parameters {
            static let StationId = "1"
        }
        
        struct ResponeKeys {
            static let TimetableParent = "timetable"
            
            struct Timetable {
                static let Arrivals = "arrivals"
                static let Departures = "departures"
            }
            
            struct TimetableItem {
                static let BriefRoute = "through_the_stations"
                static let DateTime = "datetime"
                static let Direction = "direction"
                static let LineCode = "line_code"
                static let Route = "route"
            }
            
            struct RouteItem {
                static let Id = "id"
                static let Name = "name"
                static let Address = "address"
                static let FullAddress = "full_address"
                static let Coordinates = "coordinates"
            }
            
            struct Coordinates {
                static let Latitude = "latitude"
                static let Longitude = "longitude"
            }
            
            struct DateTime {
                static let Timestamp = "timestamp"
                static let Timezone = "tz"
            }
        }
    }
    
    struct CoreData {
        struct DataModel {
            static let Name = "BusLines"
        }
        struct EntityNames {
            static let Timetable = "Timetable"
        }
    }
    
    struct ErrorMessages {
        static let NoConnection = "No internet connection. Please turn on your internet connection and try again."
        static let RequestError = "It seems that our servers can't handle your request now. Please try again later."
        static let ParsingError = "Oops. Something went wrong. Please try again later."
        static let UnauthorizedError = "It seems that you are not authorized to do this action."
        static let DefaultError = "Oops. Something went wrong. Please try again later."
    }
    
    struct UI {
        struct StationTimetableViewController {
            static let DeparturesSegmentTitle = "Departures"
            static let ArrivalsSegmentTitle = "Arrivals"
            
            struct StationTimetableTableView {
                static let CellIdentifier = "StationTimetableTableViewCell"
                static let DeparturesDirectionTitle = "To"
                static let ArrivalsDirectionTitle = "From"
            }
        }
    }
}
