//
//  DateTimeUtils.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/10/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class DateTimeUtils {
    class func formatDateTime(timeInterval: TimeInterval, dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style, timeZone: TimeZone?, dateFormat: String) -> String {
        let date = Date(timeIntervalSince1970: timeInterval)
        let dateFormatter = getDateFormatter(dateStyle: dateStyle, timeStyle: timeStyle, timeZone: timeZone, dateFormat: dateFormat)
        return dateFormatter.string(from: date)
    }
    
    private class func getDateFormatter(dateStyle: DateFormatter.Style, timeStyle: DateFormatter.Style, timeZone: TimeZone?, dateFormat: String) -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = dateStyle
        dateFormatter.timeStyle = timeStyle
        dateFormatter.timeZone = timeZone
        dateFormatter.locale = Calendar.current.locale
        dateFormatter.dateFormat = dateFormat
        return dateFormatter
    }
}
