//
//  RouteItem.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import ObjectMapper

class RouteItem: Mappable {
    var id: Int?
    var name: String?
    var address: String?
    var fullAddress: String?
    var coordinates: Coordinates?
    
    init(id: Int?, name: String?, address: String?, fullAddress: String?, coordinates: Coordinates?) {
        self.id = id
        self.name = name
        self.address = address
        self.fullAddress = fullAddress
        self.coordinates = coordinates
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        id <- map[Configuration.API.ResponeKeys.RouteItem.Id]
        name <- map[Configuration.API.ResponeKeys.RouteItem.Name]
        address <- map[Configuration.API.ResponeKeys.RouteItem.Address]
        fullAddress <- map[Configuration.API.ResponeKeys.RouteItem.FullAddress]
        coordinates <- map[Configuration.API.ResponeKeys.RouteItem.Coordinates]
    }
}
