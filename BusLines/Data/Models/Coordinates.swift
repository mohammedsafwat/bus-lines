//
//  Coordinates.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import ObjectMapper

class Coordinates: Mappable {
    var latitude: Float?
    var longitude: Float?
    
    init(latitude: Float?, longitude: Float?) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        latitude <- map[Configuration.API.ResponeKeys.Coordinates.Latitude]
        longitude <- map[Configuration.API.ResponeKeys.Coordinates.Longitude]
    }
}
