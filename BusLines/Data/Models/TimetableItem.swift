//
//  TimetableItem.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import ObjectMapper

class TimetableItem: Mappable {
    var briefRoute: String?
    var dateTime: DateTime?
    var direction: String?
    var lineCode: String?
    var routes: [RouteItem]?
    
    init(briefRoute: String?, dateTime: DateTime?, direction: String?, lineCode: String?, routes: [RouteItem]?) {
        self.briefRoute = briefRoute
        self.dateTime = dateTime
        self.direction = direction
        self.lineCode = lineCode
        self.routes = routes
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        briefRoute <- map[Configuration.API.ResponeKeys.TimetableItem.BriefRoute]
        dateTime <- map[Configuration.API.ResponeKeys.TimetableItem.DateTime]
        direction <- map[Configuration.API.ResponeKeys.TimetableItem.Direction]
        lineCode <- map[Configuration.API.ResponeKeys.TimetableItem.LineCode]
        routes <- map[Configuration.API.ResponeKeys.TimetableItem.Route]
    }
}
