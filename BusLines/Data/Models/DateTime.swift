//
//  DateTime.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import ObjectMapper

class DateTime: Mappable {
    var timestamp: TimeInterval?
    var timezone: String?
    
    init(timestamp: TimeInterval?, timezone: String?) {
        self.timestamp = timestamp
        self.timezone = timezone
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        timestamp <- map[Configuration.API.ResponeKeys.DateTime.Timestamp]
        timezone <- map[Configuration.API.ResponeKeys.DateTime.Timezone]
    }
}


