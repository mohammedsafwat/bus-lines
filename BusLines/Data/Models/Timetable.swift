//
//  Timetable.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import ObjectMapper

class Timetable: Mappable {
    var stationId: String?
    var arrivals: [TimetableItem]?
    var departures: [TimetableItem]?

    init(stationId: String?, arrivals: [TimetableItem]?, departures: [TimetableItem]?) {
        self.stationId = stationId
        self.arrivals = arrivals
        self.departures = departures
    }
    
    required init?(map: Map) {
        mapping(map: map)
    }
    
    func mapping(map: Map) {
        arrivals <- map[Configuration.API.ResponeKeys.Timetable.Arrivals]
        departures <- map[Configuration.API.ResponeKeys.Timetable.Departures]
    }
}
