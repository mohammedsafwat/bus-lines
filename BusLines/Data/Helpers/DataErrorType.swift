//
//  DataErrorType.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

enum DataErrorType {
    case noConnection
    case timeout
    case serverFailed
    case requestFailed
    case parse
    case unauthorized
    case dbFailed
    case unknown
    
    var errorMessage: String {
        switch self {
        case .noConnection:
            return Configuration.ErrorMessages.NoConnection
        case .serverFailed, .timeout, .requestFailed:
            return Configuration.ErrorMessages.RequestError
        case .parse:
            return Configuration.ErrorMessages.ParsingError
        case .unauthorized:
            return Configuration.ErrorMessages.UnauthorizedError
        default:
            return Configuration.ErrorMessages.DefaultError
        }
    }
}
