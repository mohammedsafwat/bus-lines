//
//  DataError.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class DataError {
    var type: DataErrorType
    var message: String?
    
    init(type: DataErrorType, message: String? = nil) {
        self.type = type
        self.message = message
    }
}
