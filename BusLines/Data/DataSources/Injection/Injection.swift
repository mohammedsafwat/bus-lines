//
//  Injection.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/9/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class Injection {
    class func provideStationTimetableRepository() -> StationTimetableRepository? {
        let apiClient = RESTApiClient(headers: Configuration.API.Headers)
        let requestUrl = Configuration.API.Endpoints.BaseURL + Configuration.API.Endpoints.Path
        let remoteDataSource = StationTimetableRemoteDataSource(apiClient: apiClient, requestUrl: requestUrl)
        
        let coreDataManager = CoreDataManager(modelName: Configuration.CoreData.DataModel.Name)
        let localDataSource = StationTimetableLocalDataSource(coreDataManager: coreDataManager)
        
        let stationTimetableRepository = StationTimetableRepository(remoteDataSource: remoteDataSource, localDataSource: localDataSource)
        
        return stationTimetableRepository
    }
}
