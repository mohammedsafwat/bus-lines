//
//  StationTimetableLocalDataSouce.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
import CoreData

class StationTimetableLocalDataSource: StationTimetableDataSource {
    
    // MARK: - Properties
    
    private var coreDataManager: CoreDataManager
    private let managedObjectContext: NSManagedObjectContext
    
    init(coreDataManager: CoreDataManager) {
        self.coreDataManager = coreDataManager
        self.managedObjectContext = coreDataManager.managedObjectContext()
    }
    
    // MARK: - StationTimetableDataSource Methods
    
    func fetchStationTimetable(stationId: String, onSuccess: SuccessCallBack? = nil, onError: ErrorCallBack? = nil) {
        let fetchRequest: NSFetchRequest<TimetableEntity> = TimetableEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "stationId = %@", stationId)
        
        managedObjectContext.performAndWait {
            do {
                // Execute Fetch Request
                guard let timetableManagedObject = try fetchRequest.execute().first else {
                    let deleteError = DataError(type: .dbFailed, message: "An error happend while trying to fetch a time table from local data store. ")
                    onError?(deleteError)
                    return
                }
                
                let arrivals = createTimetableItems(from: timetableManagedObject.arrivals)
                let departures = createTimetableItems(from: timetableManagedObject.departures)
                let sortedArrivals = sortTimetableItemsWithTimestamp(timetableItems: arrivals)
                let sortedDepartures = sortTimetableItemsWithTimestamp(timetableItems: departures)
                let timetable = Timetable(stationId: timetableManagedObject.stationId, arrivals: sortedArrivals, departures: sortedDepartures)
                onSuccess?(timetable)
            } catch {
                let fetchError = DataError(type: .dbFailed, message: error.localizedDescription)
                onError?(fetchError)
            }
        }
    }
    
    func saveStationTimetable(stationId: String, stationTimetable: Timetable, onError: ErrorCallBack? = nil) {
        managedObjectContext.performAndWait {
            // Create a new managed object
            let timetableManagedObject = TimetableEntity(context: managedObjectContext)
            
            // Configure the timetable managed object
            timetableManagedObject.stationId = stationId
            timetableManagedObject.arrivals = createTimetableItemManagedObjects(from: stationTimetable.arrivals)
            timetableManagedObject.departures = createTimetableItemManagedObjects(from: stationTimetable.departures)
            
            do {
                try managedObjectContext.save()
            } catch {
                let saveError = DataError(type: .dbFailed, message: error.localizedDescription)
                onError?(saveError)
            }
        }
    }
    
    func deleteStationTimetable(stationId: String, onError: ErrorCallBack? = nil) {
        let fetchRequest: NSFetchRequest<TimetableEntity> = TimetableEntity.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "stationId = %@", stationId)
        
        managedObjectContext.performAndWait {
            do {
                if let timetableManagedObject = try fetchRequest.execute().first {
                    managedObjectContext.delete(timetableManagedObject)
                    try managedObjectContext.save()
                }
                
            } catch {
                let deleteError = DataError(type: .dbFailed, message: error.localizedDescription)
                onError?(deleteError)
            }
        }
    }
    
    func deleteAllStationTimetables(onError: ErrorCallBack? = nil) {
        let fetchRequest: NSFetchRequest<TimetableEntity> = TimetableEntity.fetchRequest()
        
        managedObjectContext.performAndWait {
            do {
                let timetables = try fetchRequest.execute()
                for timetable in timetables {
                    if let stationId = timetable.stationId {
                        deleteStationTimetable(stationId: stationId)
                    }
                }
                try managedObjectContext.save()
            } catch {
                let deleteError = DataError(type: .dbFailed, message: error.localizedDescription)
                onError?(deleteError)
            }
        }
    }
    
    func refreshStationTimetable(forceUpdate: Bool) {
        // Not required because StationTimetableRepository handles the logic
        // of refreshing the tasks from all available data sources.
    }
}

// MARK: - Private Methods

extension StationTimetableLocalDataSource {
    private func createTimetableItemManagedObjects(from timetableItems: [TimetableItem]?) -> NSSet? {
        guard let timetableManagedObjects = timetableItems?.compactMap({ (timetableItem) -> TimetableItemEntity? in
            return createTimetableItemManagedObject(from: timetableItem)
        }) else {
            return nil
        }
        return NSSet(array: timetableManagedObjects)
    }
    
    private func createTimetableItemManagedObject(from timetableItem: TimetableItem) -> TimetableItemEntity {
        let timetableItemEntity = TimetableItemEntity(context: managedObjectContext)
        timetableItemEntity.briefRoute = timetableItem.briefRoute
        timetableItemEntity.dateTime = createDateTimeManagedObject(from: timetableItem.dateTime)
        timetableItemEntity.direction = timetableItem.direction
        timetableItemEntity.lineCode = timetableItem.lineCode
        
        if let routesEntities = timetableItem.routes?.compactMap({ (routeItem) -> RouteItemEntity in
            return createRouteItemManagedObject(from: routeItem)
        }) {
            timetableItemEntity.routes = NSSet(array: routesEntities)
        }
        return timetableItemEntity
    }
    
    private func createDateTimeManagedObject(from dateTime: DateTime?) -> DateTimeEntity {
        let dateTimeEntity = DateTimeEntity(context: managedObjectContext)
        dateTimeEntity.timezone = dateTime?.timezone
        if let timestamp = dateTime?.timestamp {
            dateTimeEntity.timestamp = timestamp
        }
        return dateTimeEntity
    }
    
    private func createRouteItemManagedObject(from routeItem: RouteItem?) -> RouteItemEntity {
        let routeItemEntity = RouteItemEntity(context: managedObjectContext)
        if let id = routeItem?.id {
            routeItemEntity.id = Int32(id)
        }
        routeItemEntity.name = routeItem?.name
        routeItemEntity.address = routeItem?.address
        routeItemEntity.fullAddress = routeItem?.fullAddress
        routeItemEntity.coordinates = createCoordinatesManagedObject(from: routeItem?.coordinates)
        return routeItemEntity
    }
    
    private func createCoordinatesManagedObject(from coordinates: Coordinates?) -> CoordinatesEntity {
        let coordinatesEntity = CoordinatesEntity(context: managedObjectContext)
        if let latitude = coordinates?.latitude {
            coordinatesEntity.latitude = latitude
        }
        if let longitude = coordinates?.longitude {
            coordinatesEntity.longitude = longitude
        }
        return coordinatesEntity
    }
    
    private func createTimetableItems(from timeTableItemEntities: NSSet?) -> [TimetableItem]? {
        return timeTableItemEntities?.compactMap({ (arrivalEntity) -> TimetableItem? in
            guard let arrivalEntity = arrivalEntity as? TimetableItemEntity else { return nil }
            return createTimetableItem(from: arrivalEntity)
        })
    }
    
    private func createTimetableItem(from timetableItemEntity: TimetableItemEntity) -> TimetableItem {
        let briefRoute = timetableItemEntity.briefRoute
        let dateTime = createDateTime(from: timetableItemEntity.dateTime)
        let direction = timetableItemEntity.direction
        let lineCode = timetableItemEntity.lineCode
        let routes = timetableItemEntity.routes?.compactMap({ (routeItemEntity) -> RouteItem? in
            guard let routeItemEntity = routeItemEntity as? RouteItemEntity else { return nil }
            return createRouteItem(from: routeItemEntity)
        })
        
        let timetableItem = TimetableItem(briefRoute: briefRoute, dateTime: dateTime, direction: direction, lineCode: lineCode, routes: routes)
        return timetableItem
    }
    
    private func createDateTime(from dateTimeEntity: DateTimeEntity?) -> DateTime {
        let dateTime = DateTime(timestamp: dateTimeEntity?.timestamp, timezone: dateTimeEntity?.timezone)
        return dateTime
    }
    
    private func createRouteItem(from routeItemEntity: RouteItemEntity) -> RouteItem {
        let id = Int(routeItemEntity.id)
        let name = routeItemEntity.name
        let address = routeItemEntity.address
        let fullAddress = routeItemEntity.fullAddress
        let coordinates = Coordinates(latitude: routeItemEntity.coordinates?.latitude, longitude: routeItemEntity.coordinates?.longitude)
        
        let routeItem = RouteItem(id: id, name: name, address: address, fullAddress: fullAddress, coordinates: coordinates)
        return routeItem
    }
    
    private func sortTimetableItemsWithTimestamp(timetableItems: [TimetableItem]?) -> [TimetableItem]? {
        let sortedTimetableItems = timetableItems?.sorted(by: { (firstItem: TimetableItem, secondItem: TimetableItem) -> Bool in
            if let firstTimestamp = firstItem.dateTime?.timestamp, let secondTimestamp = secondItem.dateTime?.timestamp {
                let date1 = Date(timeIntervalSince1970: firstTimestamp)
                let date2 = Date(timeIntervalSince1970: secondTimestamp)
                return date1.compare(date2) == ComparisonResult.orderedAscending
            }
            return false
        })
        return sortedTimetableItems
    }
}
