//
//  StationTimetableRepository.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class StationTimetableRepository: StationTimetableDataSource {
    
    // MARK: - Properties
    
    private var remoteDataSource: StationTimetableRemoteDataSource?
    private var localDataSource: StationTimetableLocalDataSource?
    var cachedTimetableData: [String:Timetable] = [:]
    var cacheIsDirty: Bool = false
    
    init(remoteDataSource: StationTimetableRemoteDataSource?, localDataSource: StationTimetableLocalDataSource?) {
        self.remoteDataSource = remoteDataSource
        self.localDataSource = localDataSource
    }
    
    func fetchStationTimetable(stationId: String, onSuccess: SuccessCallBack? = nil, onError: ErrorCallBack? = nil) {
        if !cachedTimetableData.isEmpty && !cacheIsDirty {
            if let timetable = cachedTimetableData[stationId] {
                onSuccess?(timetable)
                return
            }
        }
        
        if cacheIsDirty {
            getStationTimetableFromRemoteDataSource(stationId: stationId, onSuccess: { (timetable) in
                onSuccess?(timetable)
            }) { (error) in
                onError?(error)
            }
        } else {
            localDataSource?.fetchStationTimetable(stationId: stationId, onSuccess: { [weak self] (timetable) in
                self?.refreshCache(stationId: stationId, timetable: timetable)
                guard let cachedTimetable = self?.cachedTimetableData[stationId] else { return }
                onSuccess?(cachedTimetable)
            }, onError: { [weak self] (error) in
                self?.getStationTimetableFromRemoteDataSource(stationId: stationId, onSuccess: { (timetable) in
                    onSuccess?(timetable)
                }, onError: { (error) in
                    onError?(error)
                })
            })
        }
    }
    
    func saveStationTimetable(stationId: String, stationTimetable: Timetable, onError: ErrorCallBack? = nil) {
        remoteDataSource?.saveStationTimetable(stationId: stationId, stationTimetable: stationTimetable, onError: onError)
        localDataSource?.saveStationTimetable(stationId: stationId, stationTimetable: stationTimetable, onError: onError)
        cachedTimetableData[stationId] = stationTimetable
    }
    
    func deleteStationTimetable(stationId: String, onError: ErrorCallBack? = nil) {
        remoteDataSource?.deleteStationTimetable(stationId: stationId, onError: onError)
        localDataSource?.deleteStationTimetable(stationId: stationId, onError: onError)
        cachedTimetableData.removeAll()
    }
    
    func refreshStationTimetable(forceUpdate: Bool) {
        cacheIsDirty = forceUpdate
    }
}

extension StationTimetableRepository {
    private func getStationTimetableFromRemoteDataSource(stationId: String, onSuccess: SuccessCallBack?, onError: ErrorCallBack?) {
        remoteDataSource?.fetchStationTimetable(stationId: stationId, onSuccess: { [weak self] (timetable) in
            self?.refreshCache(stationId: stationId, timetable: timetable)
            self?.refreshLocalDataSource(stationId: stationId, timetable: timetable, onError: onError)
            onSuccess?(timetable)
        }, onError: { (error) in
            onError?(error)
        })
    }
    
    private func refreshCache(stationId: String, timetable: Timetable) {
        cachedTimetableData[stationId] = timetable
    }
    
    private func refreshLocalDataSource(stationId: String, timetable: Timetable, onError: ErrorCallBack?) {
        localDataSource?.deleteStationTimetable(stationId: stationId, onError: onError)
        localDataSource?.saveStationTimetable(stationId: stationId, stationTimetable: timetable, onError: onError)
    }
}
