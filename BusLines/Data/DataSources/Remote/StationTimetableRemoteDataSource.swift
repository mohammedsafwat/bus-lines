//
//  StationTimetableRemoteDataSource.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/7/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation
import SwiftyJSON

class StationTimetableRemoteDataSource: StationTimetableDataSource {
    
    // MARK: - Properties
    
    private var apiClient: ApiClient
    private var requestUrl: String
    
    init(apiClient: ApiClient, requestUrl: String) {
        self.apiClient = apiClient
        self.requestUrl = requestUrl
    }
    
    // MARK: - StationTimetableDataSource Methods
    
    func fetchStationTimetable(stationId: String, onSuccess: SuccessCallBack? = nil, onError: ErrorCallBack? = nil) {
        requestUrl = requestUrl.replacingOccurrences(of: "{station_id}", with: stationId)
        apiClient.performRequest(requestUrlString: requestUrl, onSuccess: { (data) in
            let jsonResponseDictionary = JSON(data).dictionaryValue
            guard let timetableJSON = jsonResponseDictionary[Configuration.API.ResponeKeys.TimetableParent]?.dictionaryObject, let timetable = Timetable(JSON: timetableJSON) else {
                let dataError = DataError(type: .parse, message: "An error happend while fetching station time table data.")
                onError?(dataError)
                return
            }
            timetable.stationId = stationId
            onSuccess?(timetable)
        }) { (error) in
            onError?(error)
        }
    }
    
    func saveStationTimetable(stationId: String, stationTimetable: Timetable, onError: ErrorCallBack?) {
        // Not required for now as we don't have an API endpoint to save the station timetable remotely.
    }
    
    func deleteStationTimetable(stationId: String, onError: ErrorCallBack?) {
        // Not required for now as we don't have an API endpoint to delete the station timetable remotely.
    }
    
    func refreshStationTimetable(forceUpdate: Bool) {
        // Not required because StationTimetableRepository handles the logic
        // of refreshing the station timetable from all available data sources.
    }
}
