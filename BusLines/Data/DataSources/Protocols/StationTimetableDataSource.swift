//
//  StationTimetableDataSource.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/6/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

typealias SuccessCallBack = (Timetable) -> Void
typealias ErrorCallBack = (DataError?) -> Void

protocol StationTimetableDataSource {
    func fetchStationTimetable(stationId: String, onSuccess: SuccessCallBack?, onError: ErrorCallBack?)
    func saveStationTimetable(stationId: String, stationTimetable: Timetable, onError: ErrorCallBack?)
    func deleteStationTimetable(stationId: String, onError: ErrorCallBack?)
    func refreshStationTimetable(forceUpdate: Bool)
}
