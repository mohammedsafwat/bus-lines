//
//  StationTimetableItemRepresentable.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/9/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

protocol StationTimetableItemRepresentable {
    
    var directionTitle: String { get }
    var directionDetails: String { get }
    var lineCode: String { get }
    var time: String { get }
}
