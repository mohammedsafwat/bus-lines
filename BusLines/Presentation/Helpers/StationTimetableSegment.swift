//
//  StationTimetableSegment.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/9/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

enum StationTimetableSegment: Int {
    case departures = 0
    case arrivals = 1
}
