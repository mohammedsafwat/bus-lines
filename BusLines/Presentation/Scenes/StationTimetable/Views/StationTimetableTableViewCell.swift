//
//  StationTimetableTableViewCell.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/9/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

class StationTimetableTableViewCell: UITableViewCell {
    @IBOutlet weak var fromToLabel: UILabel!
    @IBOutlet weak var directionLabel: UILabel!
    @IBOutlet weak var lineCodeLabel: UILabel!
    @IBOutlet weak var routeLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    func configure(withViewModel viewModel: StationTimetableItemViewModel?) {
        fromToLabel.text = viewModel?.directionTitle
        directionLabel.text = viewModel?.directionDetails
        lineCodeLabel.text = viewModel?.lineCode
        routeLabel.text = viewModel?.route
        timeLabel.text = viewModel?.time
    }
}
