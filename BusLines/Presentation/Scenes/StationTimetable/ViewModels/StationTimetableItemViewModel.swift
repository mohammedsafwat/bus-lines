//
//  StationTimetableItemViewModel.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/9/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class StationTimetableItemViewModel: StationTimetableItemRepresentable {
    
    // MARK: - Properties
    
    let stationTimetableItem: TimetableItem
    let stationTimetableSegment: StationTimetableSegment
    
    init(stationTimetableItem: TimetableItem, stationTimetableSegment :StationTimetableSegment) {
        self.stationTimetableItem = stationTimetableItem
        self.stationTimetableSegment = stationTimetableSegment
    }
    
    var directionTitle: String {
        return stationTimetableSegment == .departures ? "To" : "From"
    }
    
    var directionDetails: String {
        guard let direction = stationTimetableItem.direction else { return "" }
        return "Direction " + direction
    }
    
    var lineCode: String {
        guard let lineCode = stationTimetableItem.lineCode else { return "" }
        return "Line " + lineCode
    }
    
    var route: String {
        guard let routeStrings = stationTimetableItem.routes?.compactMap({ (routeItem) -> String? in
            return routeItem.name
        }) else {
            return ""
        }
        return routeStrings.joined(separator: " → ")
    }
    
    var time: String {
        guard let timezone = stationTimetableItem.dateTime?.timezone, let timestamp = stationTimetableItem.dateTime?.timestamp else {
            return ""
        }
        return DateTimeUtils.formatDateTime(timeInterval: timestamp, dateStyle: .none, timeStyle: .medium, timeZone: TimeZone(abbreviation: timezone), dateFormat: "HH:mm")
    }
    
    var date: String {
        guard let timezone = stationTimetableItem.dateTime?.timezone, let timestamp = stationTimetableItem.dateTime?.timestamp else {
            return ""
        }
        return DateTimeUtils.formatDateTime(timeInterval: timestamp, dateStyle: .medium, timeStyle: .none, timeZone: TimeZone(abbreviation: timezone), dateFormat: "E, d MMM yyyy")
    }
}
