//
//  StationTimetableViewModel.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/8/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import Foundation

class StationTimetableViewModel {
    private var stationTimetableDataSource: StationTimetableDataSource?
    
    // MARK: Initializer
    
    init(stationTimetableDataSource: StationTimetableDataSource?) {
        self.stationTimetableDataSource = stationTimetableDataSource
    }
    
    // MARK: - Data Binding Closures
    
    var showErrorMessage: ((String?) -> Void)?
    var updateLoadingStatus: ((Bool) -> Void)?
    var reloadTimetable: (() -> Void)?
    
    // MARK: - Properties
    
    var selectedStationTimetableSegment: StationTimetableSegment?
    
    var dataLoadingError: DataError? {
        didSet {
            showErrorMessage?(dataLoadingError?.message)
        }
    }
    
    var dataLoading: Bool = false {
        didSet {
            updateLoadingStatus?(dataLoading)
        }
    }
    
    var timetable: Timetable? {
        didSet {
            reloadTimetable?()
        }
    }
    
    var numberOfSections: Int {
        return 1
    }
    
    var numberOfRows: Int {
        guard let selectedStationTimetableSegment = selectedStationTimetableSegment else { return 0 }
        switch selectedStationTimetableSegment {
        case .departures:
            guard let departures = timetable?.departures else { return 0 }
            return departures.count
        case .arrivals:
            guard let arrivals = timetable?.arrivals else { return 0 }
            return arrivals.count
        }
    }
    
    // MARK: - Helper Methods
    
    func fetchStationTimetable(stationId: String, forceUpdate: Bool, showLoadingUI: Bool) {
        if showLoadingUI {
            dataLoading = true
        }
        
        // Decide if we should set the cache to be dirty and start fetching from the APIs.
        // If `forceUpdate` is true, then it means that we will mark the cache as dirty
        // and fetch the data from the APIs again.
        stationTimetableDataSource?.refreshStationTimetable(forceUpdate: forceUpdate)
        
        stationTimetableDataSource?.fetchStationTimetable(stationId: stationId, onSuccess: { [weak self] (timetable) in
            self?.timetable = timetable
            self?.dataLoading = false
        }) { [weak self] (dataError) in
            self?.dataLoadingError = dataError
            self?.dataLoading = false
        }
    }
    
    func stationTimetableItemViewModel(for index: Int) -> StationTimetableItemViewModel? {
        guard let selectedStationTimetableSegment = selectedStationTimetableSegment else { return nil }
        switch selectedStationTimetableSegment {
        case .departures:
            guard let departures = timetable?.departures else { return nil }
            return StationTimetableItemViewModel(stationTimetableItem: departures[index], stationTimetableSegment: .departures)
        case .arrivals:
            guard let arrivals = timetable?.arrivals else { return nil }
            return StationTimetableItemViewModel(stationTimetableItem: arrivals[index], stationTimetableSegment: .arrivals)
        }
    }
}
