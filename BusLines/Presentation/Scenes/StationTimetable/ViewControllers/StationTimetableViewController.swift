//
//  StationTimetableViewController.swift
//  BusLines
//
//  Created by Mohammed Safwat on 6/8/18.
//  Copyright © 2018 Mohammed Safwat. All rights reserved.
//

import UIKit

class StationTimetableViewController: UIViewController {
    @IBOutlet weak var timetableSegmentedControl: UISegmentedControl!
    @IBOutlet weak var timetableTableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    lazy var viewModel: StationTimetableViewModel = {
        return StationTimetableViewModel(stationTimetableDataSource: Injection.provideStationTimetableRepository())
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupUIDataBinding()
        setSelectedStationTimetableSegment(segment: .departures)
        fetchStationTimetable(stationId: Configuration.API.Parameters.StationId, forceUpdate: true, showLoadingUI: true)
    }
    
    // MARK: - IBActions
    
    @IBAction func didChangeTimetableSegment(_ sender: Any) {
        guard let selectedStationTimetableSegment = StationTimetableSegment(rawValue: timetableSegmentedControl.selectedSegmentIndex) else {
            return
        }
        setSelectedStationTimetableSegment(segment: selectedStationTimetableSegment)
        fetchStationTimetable(stationId: Configuration.API.Parameters.StationId, forceUpdate: false, showLoadingUI: true)
    }
    
    @IBAction func didTapRefreshButton(_ sender: Any) {
        fetchStationTimetable(stationId: Configuration.API.Parameters.StationId, forceUpdate: true, showLoadingUI: true)
    }
}

// MARK: - UITableViewDataSource Methods

extension StationTimetableViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Configuration.UI.StationTimetableViewController.StationTimetableTableView.CellIdentifier, for: indexPath) as? StationTimetableTableViewCell else { fatalError("Unexpected Table View Cell") }
        cell.configure(withViewModel: viewModel.stationTimetableItemViewModel(for: indexPath.row))
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows
    }
}

// MARK: - Private Methods

extension StationTimetableViewController {
    private func setupUIDataBinding() {
        viewModel.showErrorMessage = { [unowned self] (errorMessage) in
            UIAlertUtils.showAlert(inViewController: self, withMessage: errorMessage)
        }
        
        viewModel.updateLoadingStatus = { [unowned self] (dataLoading) in
            if dataLoading {
                self.activityIndicator.isHidden = false
                self.activityIndicator.startAnimating()
            } else {
                self.activityIndicator.isHidden = true
                self.activityIndicator.stopAnimating()
            }
        }
        
        viewModel.reloadTimetable = { [unowned self] in
            self.timetableTableView.reloadData()
        }
    }
    
    private func setupUI() {
        setupTimetableTableView()
        setupSegmentedControlTitles()
    }
    
    private func setupTimetableTableView() {
        timetableTableView.dataSource = self
        timetableTableView.rowHeight = UITableViewAutomaticDimension
        timetableTableView.tableFooterView = UIView()
    }
    
    private func setupSegmentedControlTitles() {
        timetableSegmentedControl.setTitle(Configuration.UI.StationTimetableViewController.DeparturesSegmentTitle, forSegmentAt: 0)
        timetableSegmentedControl.setTitle(Configuration.UI.StationTimetableViewController.ArrivalsSegmentTitle, forSegmentAt: 1)
    }
    
    private func setSelectedStationTimetableSegment(segment: StationTimetableSegment) {
        viewModel.selectedStationTimetableSegment = segment
    }
    
    private func fetchStationTimetable(stationId: String, forceUpdate: Bool, showLoadingUI: Bool) {
        viewModel.fetchStationTimetable(stationId: stationId, forceUpdate: forceUpdate, showLoadingUI: showLoadingUI)
    }
}
